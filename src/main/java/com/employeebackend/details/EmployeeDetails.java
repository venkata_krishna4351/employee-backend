package com.employeebackend.details;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EmployeeDetails {
    private String sex;
    private String salary;
    private String employeePhoneNumber;
    private String employeeId;
    private String employeeDepartment;
    private String employeeTitle;
    private String employeeBranch;
}
