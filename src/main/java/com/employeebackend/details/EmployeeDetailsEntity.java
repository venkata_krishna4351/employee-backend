package com.employeebackend.details;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "employee_details")
public class EmployeeDetailsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String sex;
    private String salary;
    private String employeePhoneNumber;
    private String employeeId;
    private String employeeDepartment;
    private String employeeTitle;
    private String employeeBranch;
}
