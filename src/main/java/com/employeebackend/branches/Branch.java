package com.employeebackend.branches;

import com.employeebackend.address.Address;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Branch {
    private String branchCode;
    private String branchHead;
    private Address address;
}
