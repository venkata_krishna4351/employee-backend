package com.employeebackend.branches;


import com.employeebackend.address.AddressEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "branch")
public class BranchEntity {
    @Id
    private String branchCode;
    private String branchHead;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, targetEntity = AddressEntity.class)
    @JoinColumn(name = "address_id")
    private AddressEntity addressEntity;
}
