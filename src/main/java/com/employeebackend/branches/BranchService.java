package com.employeebackend.branches;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class BranchService {

    BranchRepository branchRepository;
    BranchTransformer branchTransformer;

    public Branch saveBranch(Branch branch) {
        return branchTransformer.fromEntity(branchRepository.save(branchTransformer.toEntity(branch)));
    }

    public Optional<BranchEntity> findBranchWithBranchCode(String branchCode){
        return branchRepository.findByBranchCode(branchCode);
    }
}
