package com.employeebackend.branches;

import com.employeebackend.address.Address;
import com.employeebackend.address.AddressEntity;
import org.springframework.stereotype.Component;

@Component
public class BranchTransformer {

    public BranchEntity toEntity(Branch branch){
        return BranchEntity.builder()
                .branchHead(branch.getBranchHead())
                .branchCode(branch.getBranchCode())
                .addressEntity(AddressEntity.builder()
                        .streetName(branch.getAddress().getStreetName())
                        .aptNo(branch.getAddress().getAptNo())
                        .city(branch.getAddress().getCity())
                        .state(branch.getAddress().getState())
                        .zip(branch.getAddress().getZip())
                        .build())
                .build();
    }

    public Branch fromEntity(BranchEntity branchEntity){
        return Branch.builder()
                .branchHead(branchEntity.getBranchHead())
                .branchCode(branchEntity.getBranchCode())
                .address(Address.builder()
                        .streetName(branchEntity.getAddressEntity().getStreetName())
                        .aptNo(branchEntity.getAddressEntity().getAptNo())
                        .city(branchEntity.getAddressEntity().getCity())
                        .state(branchEntity.getAddressEntity().getState())
                        .zip(branchEntity.getAddressEntity().getZip())
                        .build())
                .build();
    }

}
