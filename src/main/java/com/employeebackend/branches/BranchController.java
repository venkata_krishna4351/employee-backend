package com.employeebackend.branches;

import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/v1/branch/service")
@AllArgsConstructor
public class BranchController {

    BranchService branchService;

    @PostMapping(path="/add/branch", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Branch addNewBranch(@RequestBody Branch branch){
        return branchService.saveBranch(branch);
    }

    @GetMapping(path = "/status")
    public String getStatus(){
        return "Working";
    }
}
