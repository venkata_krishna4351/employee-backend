package com.employeebackend.branches;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BranchRepository extends JpaRepository<BranchEntity, String> {
    Optional<BranchEntity> findByBranchCode(String branchCode);
}
