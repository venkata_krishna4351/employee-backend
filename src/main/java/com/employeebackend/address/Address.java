package com.employeebackend.address;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Address {
    private String streetName;
    private String aptNo;
    private String city;
    private String state;
    private String zip;
}
