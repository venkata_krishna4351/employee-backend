package com.employeebackend.Employee.messaging;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EmployeeEventDetails {
    private String firstName;
    private String lastName;
    private String email;
    private String sex;
    private String salary;
    private String employeePhoneNumber;
    private String employeeId;
    private String employeeDepartment;
    private String employeeTitle;
    private String streetName;
    private String aptNo;
    private String city;
    private String state;
    private String zip;
}
