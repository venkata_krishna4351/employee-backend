package com.employeebackend.Employee;

import com.employeebackend.Employee.messaging.EmployeeEvent;
import com.employeebackend.branches.BranchEntity;
import com.employeebackend.branches.BranchService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class EmployeeService {

    EmployeeTransformer employeeTransformer;
    EmployeeRepository employeeRepository;
    BranchService branchService;

    public void addEmployees(EmployeeEvent employeeEvent) {
        Optional<BranchEntity> branchEntity = branchService.findBranchWithBranchCode(employeeEvent.getBranchName());
        if(branchEntity.isPresent()){
            employeeRepository.saveAll(employeeTransformer.toEntityFromEvent(employeeEvent));
        }else{
            throw new RuntimeException("Branch Not found");
        }

    }
}
