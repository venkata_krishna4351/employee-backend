package com.employeebackend.Employee;

import com.employeebackend.address.AddressEntity;
import com.employeebackend.details.EmployeeDetailsEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "employee")
public class EmployeeEntity {
    private String firstName;
    private String lastName;
    @Id
    private String email;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, targetEntity = EmployeeDetailsEntity.class)
    @JoinColumn(name = "employee_details_id")
    private EmployeeDetailsEntity employeeDetailsEntity;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, targetEntity = AddressEntity.class)
    @JoinColumn(name = "address_id")
    private AddressEntity addressEntity;
}
