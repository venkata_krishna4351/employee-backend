package com.employeebackend.Employee;

import com.employeebackend.address.Address;
import com.employeebackend.details.EmployeeDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Employee {
    private String firstName;
    private String lastName;
    private String email;
    private EmployeeDetails employeeDetails;
    private Address address;
}
