package com.employeebackend.Employee;

import com.employeebackend.Employee.messaging.EmployeeEvent;
import com.employeebackend.address.AddressEntity;
import com.employeebackend.details.EmployeeDetailsEntity;
import org.springframework.stereotype.Component;
import java.util.List;


import java.util.stream.Collectors;

@Component
public class EmployeeTransformer {

    public List<EmployeeEntity> toEntityFromEvent(EmployeeEvent employeeEvent){
        return employeeEvent.getDetails().stream().map(e-> EmployeeEntity.builder()
                .firstName(e.getFirstName())
                .lastName(e.getLastName())
                .email(e.getEmail())
                .addressEntity(AddressEntity.builder()
                        .aptNo(e.getAptNo())
                        .streetName(e.getStreetName())
                        .city(e.getCity())
                        .state(e.getState())
                        .zip(e.getZip())
                        .build())
                .employeeDetailsEntity(EmployeeDetailsEntity.builder()
                        .employeeBranch(employeeEvent.getBranchName())
                        .employeeDepartment(e.getEmployeeDepartment())
                        .employeeId(e.getEmployeeId())
                        .employeePhoneNumber(e.getEmployeePhoneNumber())
                        .employeeTitle(e.getEmployeeTitle())
                        .salary(e.getSalary())
                        .sex(e.getSex())
                        .build())
                .build()).collect(Collectors.toList());
    }
}
