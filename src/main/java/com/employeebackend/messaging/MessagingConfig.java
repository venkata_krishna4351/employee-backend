package com.employeebackend.messaging;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.MessageChannel;

@Configuration
@EnableBinding(MessagingConfig.Channels.class)
public class MessagingConfig {

    public static final String STREAM_EMPLOYEE_INPUT = "streamEmployeeData";
    public interface Channels{

        @Input(STREAM_EMPLOYEE_INPUT)
        MessageChannel streamEmployeeData();
    }
}
