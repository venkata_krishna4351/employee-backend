package com.employeebackend.messaging;

import com.employeebackend.Employee.EmployeeService;
import com.employeebackend.Employee.messaging.EmployeeEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class MessagingListenerService {

    MessagingConfig.Channels channels;
    EmployeeService employeeService;

    @StreamListener(MessagingConfig.STREAM_EMPLOYEE_INPUT)
    public void readEmployeeData(@Payload EmployeeEvent employeeEvent){
        employeeService.addEmployees(employeeEvent);

    }
}
