CREATE TABLE IF NOT EXISTS  employee_details(
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    sex varchar(225) NULL,
    salary varchar(225)  NULL,
    employee_phone_number varchar(225)  NULL,
    employee_id varchar(225)  NULL,
    employee_department varchar(225) NULL,
    employee_title varchar(225) NULL,
    employee_branch varchar(225) NULL
);

CREATE TABLE IF NOT EXISTS  address(
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    street_name varchar(225) NOT NULL,
    apt_no varchar(225) NOT NULL,
    city varchar(225) NOT NULL,
    state varchar(225) NOT NULL,
    zip varchar(225) NOT NULL
);

CREATE TABLE IF NOT EXISTS  employee(
    first_name varchar(225) NOT NULL,
    last_name varchar(225) NOT NULL,
    email varchar(225) NOT NULL PRIMARY KEY,
    employee_details_id Integer NOT NULL,
    address_id Integer NOT NULL,
    FOREIGN KEY (employee_details_id) REFERENCES employee_details(id),
    FOREIGN KEY (address_id) REFERENCES address(id)
);








