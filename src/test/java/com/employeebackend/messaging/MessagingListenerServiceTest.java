package com.employeebackend.messaging;

import com.employeebackend.Employee.EmployeeService;
import com.employeebackend.Employee.messaging.EmployeeEvent;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import static com.employeebackend.Employee.EmployeeDataFactory.validEmployeeEvent;
import static org.mockito.Mockito.verify;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class MessagingListenerServiceTest {

    @InjectMocks
    MessagingListenerService messagingListenerService;

    @Mock
    EmployeeService employeeService;

    @Mock
    MessagingConfig.Channels channels;

    @Test
    void readEmployeeData() {

        //given
        EmployeeEvent employeeEvent = validEmployeeEvent();

        //when
        messagingListenerService.readEmployeeData(employeeEvent);

        //then
        verify(employeeService).addEmployees(employeeEvent);
    }
}