package com.employeebackend.Employee;

import com.employeebackend.Employee.messaging.EmployeeEvent;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static com.employeebackend.Employee.EmployeeDataFactory.validEmployeeEntity;
import static com.employeebackend.Employee.EmployeeDataFactory.validEmployeeEvent;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class EmployeeTransformerTest {

    @InjectMocks
    EmployeeTransformer employeeTransformer;

    @Test
    void toEntityFromEvent() {
        //given
        EmployeeEntity employeeEntity = validEmployeeEntity();
        EmployeeEvent employeeEvent = validEmployeeEvent();

        //when
        List<EmployeeEntity> result = employeeTransformer.toEntityFromEvent(employeeEvent);

        //then
        assertThat(result).isEqualTo(singletonList(employeeEntity));

    }
}