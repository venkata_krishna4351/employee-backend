package com.employeebackend.Employee;

import com.employeebackend.Employee.messaging.EmployeeEvent;
import com.employeebackend.Employee.messaging.EmployeeEventDetails;
import com.employeebackend.address.Address;
import com.employeebackend.address.AddressEntity;
import com.employeebackend.details.EmployeeDetails;
import com.employeebackend.details.EmployeeDetailsEntity;

import static java.util.Collections.*;

public class EmployeeDataFactory {

    private EmployeeDataFactory(){}

    public static EmployeeEvent validEmployeeEvent(){
        return EmployeeEvent.builder()
                .branchName("Aus001")
                .details(singletonList(EmployeeEventDetails.builder()
                        .employeeId("1234")
                        .aptNo("2000")
                        .streetName("La Street")
                        .city("Austin")
                        .state("TX")
                        .zip("72401")
                        .email("krishna@gmail.com")
                        .employeeDepartment("Test")
                        .employeePhoneNumber("1234567890")
                        .employeeTitle("Developer")
                        .firstName("Krishna")
                        .lastName("Venkata")
                        .salary("50000")
                        .sex("M")
                        .build()))
                .build();
    }

    public static EmployeeEntity validEmployeeEntity(){
        return EmployeeEntity.builder()
                .firstName("Krishna")
                .lastName("Venkata")
                .email("krishna@gmail.com")
                .employeeDetailsEntity(EmployeeDetailsEntity.builder()
                        .sex("M")
                        .employeeId("1234")
                        .employeeBranch("Aus001")
                        .salary("50000")
                        .employeeTitle("Developer")
                        .employeePhoneNumber("1234567890")
                        .employeeDepartment("Test")
                        .build())
                .addressEntity(AddressEntity.builder()
                        .streetName("La Street")
                        .aptNo("2000")
                        .city("Austin")
                        .state("TX")
                        .zip("72401")
                        .build())
                .build();
    }

    public static Employee validEmployee(){
        return Employee.builder()
                .firstName("Krishna")
                .lastName("Venkata")
                .email("krishna@gmail.com")
                .employeeDetails(EmployeeDetails.builder()
                        .sex("M")
                        .employeeId("1234")
                        .employeeBranch("Aus001")
                        .salary("50000")
                        .employeeTitle("Developer")
                        .employeePhoneNumber("1234567890")
                        .employeeDepartment("Test")
                        .build())
                .address(Address.builder()
                        .streetName("La Street")
                        .aptNo("2000")
                        .city("Austin")
                        .state("TX")
                        .zip("72401")
                        .build())
                .build();
    }

}
